
import time
import tensorclues as tc

### Load MNIST dataset

train_images = tc.load_npy( '../data/mnist/train_images' )
train_labels = tc.load_npy( '../data/mnist/train_labels' )
test_images  = tc.load_npy( '../data/mnist/test_images'  )
test_labels  = tc.load_npy( '../data/mnist/test_labels'  )

### Start Recipe

rc = tc.Recipe()

### Create Placeholders

input = rc.placeholder( train_images.shape , first_none = True )
label = rc.placeholder( train_labels.shape , first_none = True )

### Layer Variables

wgts = tc.init( tc.vars.trunc_normal , stddev = 0.05 )
bias = tc.init( tc.vars.trunc_normal , stddev = 0.05 )

rc.defLayer( wgts = wgts , bias = bias )

### Extra Functions

actv , drop = tc.activs.relu , tc.extras.dropout
pool = tc.init( tc.extras.pool2D , ksize = 2 , strides = 2 )

rc.defLayer( post = [ pool , actv ] , strides = 1 )

### Create Hidden Layers

layer1 = rc.Conv2D( out_channels =  64 , ksize = 5 , input = input , in_channels = 1 )
layer3 = rc.Conv2D( out_channels = 128 , ksize = 5 )

layer4 = rc.Fully( out_channels = 1024 , post = [ actv , drop ] )

### Create Dropout Dictionaries

drop_train = { layer4['dropout'] : 0.5 }
drop_test  = { layer4['dropout'] : 1.0 }

### Create Output Layer

logits = rc.Fully( out_channels = label , post = None )
output = tc.activs.softmax( logits )

### Create Operations

op_loss = rc.operation( tc.ops.mean_soft_cross , logits = logits , labels = label )
op_eval = rc.operation( tc.ops.perc_correct , input1 = output , input2 = label )
op_optm = rc.optimizer( tc.optims.adam , op_loss , learning_rate = 1e-4 )

### Create Model Saver

saver = rc.saver( path = 'model_cnn2' )

### Initialize Variables

rc.initialize()
rc.print_trainable()

### Prepare Train Batch

train_batch = tc.batch.vector( train_images , train_labels )
num_batches = train_batch.num_batches( 1000 )

### Test Dictionary

dict_test = tc.merge_dicts( { input : test_images ,
                              label : test_labels } , drop_test )

### Train Loop

num_epochs = 1000
for epoch in range( num_epochs ):

    train_batch.shuffle()
    start = time.time()

    train_loss = 0
    for batch in range( num_batches ):

        batch_images , batch_labels = train_batch.next()
        dict_batch = tc.merge_dicts( { input : batch_images ,
                                       label : batch_labels } , drop_train )

        _ , batch_loss = rc.run( [ op_optm , op_loss ] , dict_batch )
        train_loss += batch_loss

    train_loss /= num_batches
    test_loss , test_eval = rc.run( [ op_loss , op_eval ] , dict_test )
    finish = time.time()

    print( 'Epoch %3d/%3d | TRAIN: Loss %6f | TEST: Loss %6f Eval %6f | TIME: %6f s'  %
                ( epoch , num_epochs , train_loss , test_loss , test_eval , finish - start ) )

    rc.save( saver )
