
import time
import tensorclues as tc

### Load MNIST dataset

train_images = tc.load_npy( '../data/mnist/train_images' )
train_labels = tc.load_npy( '../data/mnist/train_labels' )
test_images  = tc.load_npy( '../data/mnist/test_images'  )
test_labels  = tc.load_npy( '../data/mnist/test_labels'  )

### Start Recipe

rc = tc.Recipe()

### Create Placeholders

input = rc.placeholder( train_images.shape , first_none = True )
label = rc.placeholder( train_labels.shape , first_none = True )

### Layer Variables

wgts = tc.init( tc.vars.trunc_normal , stddev = 0.05 )
bias = tc.init( tc.vars.trunc_normal , stddev = 0.05 )

rc.defLayer( wgts = wgts , bias = bias )

### Extra Functions

actv , drop = tc.activs.relu , tc.extras.dropout

### Create Hidden Layers

layer1 = rc.Fully( out_channels =  256  , post = [ actv ] , input = input )
layer2 = rc.Fully( out_channels =  512  , post = [ actv , drop ] )
layer3 = rc.Fully( out_channels = 1024  , post = [ actv , drop ] )

### Create Output Layer

logits = rc.Fully( out_channels = label , post = None )
output = tc.activs.softmax( logits )

### Create Operations

op_loss = rc.operation( tc.ops.mean_soft_cross , logits = logits , labels = label )
op_eval = rc.operation( tc.ops.perc_correct , input1 = output , input2 = label )
op_optm = rc.optimizer( tc.optims.adam , op_loss , learning_rate = 1e-5 )

### Create Model Saver

saver = rc.saver( path = 'model_nn2' )

### Initialize Variables

rc.initialize()

### Prepare Train Batch

train_batch = tc.batch.vector( train_images , train_labels )
num_batches = train_batch.num_batches( 500 )

### Test Dictionary

dict_test = { input : test_images , label : test_labels ,
              layer2['dropout'] : 1.0 , layer3['dropout'] : 1.0 }

### Train Loop

num_epochs = 1000
for epoch in range( num_epochs ):

    train_batch.shuffle()
    start = time.time()

    train_loss = 0
    for batch in range( num_batches ):

        batch_images , batch_labels = train_batch.next() # Get Next Batch
        dict_batch = { input : batch_images , label : batch_labels ,
                       layer2['dropout'] : 0.5 , layer3['dropout'] : 0.5 }

        _ , batch_loss = rc.run( [ op_optm , op_loss ] , dict_batch )
        train_loss += batch_loss

    train_loss /= num_batches
    test_loss , test_eval = rc.run( [ op_loss , op_eval ] , dict_test )
    finish = time.time()

    print( 'Epoch %3d/%3d | TRAIN: Loss %6f | TEST: Loss %6f Eval %6f | TIME: %6f s'  %
                ( epoch , num_epochs , train_loss , test_loss , test_eval , finish - start ) )

    rc.save( saver )
