

import scipy.misc
import numpy as np
from glob import glob
import tensorflow as tf

### Load Image
def load_image( path , name , is_gray = False ):

    if is_gray:
        image = scipy.misc.imread( path + '/' + name , mode = 'L' ).astype( np.float )
    else:
        image = scipy.misc.imread( path + '/' + name , mode = 'RGB' ).astype( np.float )
    return image

### Load Images
def load_images( path , files , is_gray = False ):

    images = [ load_image( path , file , is_gray ) for file in files ]
    return np.array( images ).astype( np.float32 )
