
import sys
import time
import random
import scipy.misc
import numpy as np
from matplotlib import pyplot as plt

import tensorflow as tf
import tensorclues as tc

scale = 0.20
dims = [ 128 , 128 ]

img = scipy.misc.imread( 'data/image_00.png' , mode = 'RGB' )
lbl = scipy.misc.imread( 'data/label_00.png' , mode = 'L'   )

img = scipy.misc.imresize( img , size = scale )
lbl = scipy.misc.imresize( lbl , size = scale )

img = np.array( img )#.astype( np.float ) / 255.0
lbl = np.array( lbl )#.astype( np.float ) / 255.0

images , labels = [] , []
for i in range( 0 , img.shape[0] - dims[0] , int( dims[0] / 2 ) ):
    for j in range( 0 , img.shape[1] - dims[1] , int( dims[1] / 2 ) ):
        images.append( img[ i : i + dims[0] , j : j + dims[1] , : ] )
        labels.append( lbl[ i : i + dims[0] , j : j + dims[1] ] )
images , labels = np.array( images ) , np.array( labels )

for i in range( images.shape[0] ):
    scipy.misc.imsave( 'data/128x128/images/image_%04d.jpg' % ( i ) ,  images[i] )
    scipy.misc.imsave( 'data/128x128/labels/image_%04d.jpg' % ( i ) ,  labels[i] )




#n = 0#int( sys.argv[1] )
#train_images , train_labels = images[31:32] , labels[31:32]
#test_images  , test_labels  = images[31:32] , labels[31:32]

#rc = tc.Recipe()

#input = rc.placeholder( train_images.shape , first_none = True , name = 'Input' )
#label = rc.placeholder( train_labels.shape , first_none = True , name = 'Label' )

#actv , sigm , drop = tc.activs.relu , tc.activs.sigmoid , tc.extras.dropout
#pool = tc.init( tc.extras.pool2D , ksize = 2 , strides = 2 )

#conv1 = rc.Conv2D( out_channels =  32 , ksize = 5 , post = [ actv , pool ] , input = input , in_channels = 3 )
#conv2 = rc.Conv2D( out_channels =  64 , ksize = 5 , post = [ actv , pool ] )
#conv3 = rc.Conv2D( out_channels = 128 , ksize = 5 , post = [ actv , pool ] )
#conv4 = rc.Conv2D( out_channels = 128 , ksize = 3 , post = [ actv , pool ] )

#full1 = rc.Fully( out_channels = 1024            , post = [ actv , drop ] )
#logit = rc.Fully( out_channels = np.prod( dims ) , post = [        sigm ] )['x']
#logit = tf.reshape( logit , [ -1 , dims[0] , dims[1] ] )

#op_loss = tf.square( logit - label )
#op_loss = tf.reduce_mean( tf.reduce_sum( op_loss , axis = [ 1 , 2 ] ) )

#op_optm = rc.optimizer( tc.optims.adam , op_loss , learning_rate = 1e-4 )

#rc.initialize()

#### Prepare Train Batch

#train_batch = tc.batch.vector( train_images , train_labels )
#num_batches = train_batch.num_batches( 1 )

#### Train Loop

#num_epochs = 1000
#for epoch in range( num_epochs ):

#    train_batch.shuffle()
#    start = time.time()

#    train_loss = 0
#    for batch in range( num_batches ):

#        batch_images , batch_labels = train_batch.next()
#        _ , batch_loss = rc.run( [ op_optm , op_loss ] ,
#                    { input : batch_images , label : batch_labels , full1['dropout'] : 0.5 } )
#        train_loss += batch_loss

#    train_loss /= num_batches
#    finish = time.time()

#    test_image , test_label = test_images[n:n+1] , test_labels[n:n+1]
#    test_recon , test_loss = rc.run( [ logit , op_loss ] ,
#                    { input : test_image , label : test_label , full1['dropout'] : 1.0 } )

#    print( 'Epoch %3d/%3d | TRAIN: Loss %6.3f | TEST: Loss %6.3f | TIME: %6f s'  %
#                ( epoch , num_epochs , train_loss , test_loss , finish - start ) )

#    plt.subplot(1,3,1)
#    plt.imshow( test_image[0] , cmap = None )

#    plt.subplot(1,3,2)
#    plt.imshow( test_label[0] , cmap = 'gray' )

#    plt.subplot(1,3,3)
#    plt.imshow( test_recon[0] , cmap = 'gray' )

#    plt.pause( 0.01 )



##n = 100

##plt.subplot(1,2,1)
##plt.imshow( images[n] , cmap = None )

##plt.subplot(1,2,2)
##plt.imshow( labels[n] , cmap = 'gray' )

##plt.show()




#for n in range( images.shape[0] ):

#    plt.subplot(1,2,1)
#    plt.imshow( images[n] , cmap = None , vmin = 0.0 , vmax = 1.0 )

#    plt.subplot(1,2,2)
#    plt.imshow( labels[n] , cmap = 'gray' , vmin = 0.0 , vmax = 1.0 )

#    plt.pause( 0.1 )



