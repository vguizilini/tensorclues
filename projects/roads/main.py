
import os
import sys
import time
import random
import scipy.misc
import numpy as np
from matplotlib import pyplot as plt
from glob import glob

from auxiliar import *
import tensorflow as tf
import tensorclues as tc

num_epochs = 50000
batch_size_train = 2
batch_size_test  = 1
save_step = 10

dataset = '128x128'
path_train_images , path_train_labels = 'train_images' , 'train_labels'
path_test_images  , path_test_labels  = 'test_images' , 'test_labels'

path_test_recons = path_test_labels + '_recons'
path_train_images = 'data/%s/%s' % ( dataset , path_train_images )
path_train_labels = 'data/%s/%s' % ( dataset , path_train_labels )
path_test_images  = 'data/%s/%s' % ( dataset , path_test_images  )
path_test_labels  = 'data/%s/%s' % ( dataset , path_test_labels  )
path_test_recons  = 'data/%s/%s' % ( dataset , path_test_recons  )

train_strings = glob( '{}/*.*'.format( path_train_images ) )
test_strings  = glob( '{}/*.*'.format( path_test_images  ) )

is_gray_image , is_gray_label = False , True

for i in range( len( train_strings ) ): train_strings[i] = train_strings[i][-14:]
for i in range( len(  test_strings ) ):  test_strings[i] =  test_strings[i][-14:]

shape_image = [ None ] + list( load_image( path_train_images , train_strings[0] , is_gray_image ).shape )
shape_label = [ None ] + list( load_image( path_train_labels , train_strings[0] , is_gray_label ).shape )

rc = tc.Recipe()

input = rc.placeholder( shape_image , first_none = True , name = 'Input' )
label = rc.placeholder( shape_label , first_none = True , name = 'Label' )

actv , sigm , drop = tc.activs.relu , tc.activs.sigmoid , tc.extras.dropout
pool = tc.init( tc.extras.pool2D , ksize = 2 , strides = 2 )

conv1 = rc.Conv2D( out_channels =  32 , ksize = 5 , post = [ actv , pool ] , input = input )
conv2 = rc.Conv2D( out_channels =  64 , ksize = 5 , post = [ actv , pool ] )
conv3 = rc.Conv2D( out_channels = 128 , ksize = 5 , post = [ actv , pool ] )

latent = rc.Conv2D( out_channels = 128 , ksize = 3 , strides = 2 , post = [ actv , drop ] )

deconv3 = rc.Deconv2D( out_channels = 128 , strides = 2 , post = [ actv ] )
deconv2 = rc.Deconv2D( out_channels =  64 , strides = 2 , post = [ actv ] )
deconv1 = rc.Deconv2D( out_channels =  32 , strides = 2 , post = [ actv ] )

output = rc.Deconv2D( out_channels = 1 , strides = 2 , post = [ sigm ] )
output = tf.squeeze( output['x'] , axis = 3 )

#op_loss = tf.reduce_mean( tf.reduce_sum( tf.square( output - label ) , axis = [ 1 , 2 ] ) )
op_loss = - tf.reduce_mean( label * tf.log( output + 1e-6 ) + ( 1 - label ) * tf.log( 1 - output + 1e-6 ) )
op_optm = rc.optimizer( tc.optims.adam , op_loss , learning_rate = 1e-4 )

rc.initialize()

num_batches_train = len( train_strings ) // batch_size_train
num_batches_test  = len(  test_strings ) // batch_size_test

for epoch in range( num_epochs ):

    random.shuffle( train_strings )
    start = time.time()

    train_loss = 0
    for batch in range( num_batches_train ):

        st , fn = ( batch ) * batch_size_train , ( batch + 1 ) * batch_size_train
        batch_images = load_images( path_train_images , train_strings[st:fn] , is_gray_image ) / 255.0
        batch_labels = load_images( path_train_labels , train_strings[st:fn] , is_gray_label ) / 255.0
        dict_batch = { input : batch_images , label : batch_labels , latent['dropout'] : 0.5 }

        _ , batch_loss = rc.run( [ op_optm , op_loss ] , dict_batch )
        train_loss += batch_loss

    train_loss /= num_batches_train

    test_loss = 0
    for batch in range( num_batches_test ):

        st , fn = ( batch ) * batch_size_test , ( batch + 1 ) * batch_size_test
        batch_images = load_images( path_test_images , test_strings[st:fn] , is_gray_image ) / 255.0
        batch_labels = load_images( path_test_labels , test_strings[st:fn] , is_gray_label ) / 255.0
        dict_batch = { input : batch_images , label : batch_labels , latent['dropout'] : 1.0 }

        batch_recons , batch_loss = rc.run( [ output , op_loss ] , dict_batch )
        test_loss += batch_loss

        if epoch % ( save_step + 1 ) == 0:

            for i in range( batch_images.shape[0] ):

                plt.subplot(1,3,1)
                plt.imshow( batch_images[i] , cmap = None   )
                plt.subplot(1,3,2)
                plt.imshow( batch_labels[i] , cmap = 'gray' , vmin = 0.0 , vmax = 1.0 )
                plt.subplot(1,3,3)
                plt.imshow( batch_recons[i] , cmap = 'gray' , vmin = 0.0 , vmax = 1.0 )

                curr_path = path_test_recons + '/epoch_' + str( epoch ) + '/'
                if not os.path.exists( curr_path ): os.makedirs( curr_path )
                plt.savefig( curr_path + test_strings[ st + i ] )

    test_loss /= num_batches_test

    finish = time.time()
    print( 'Epoch %3d/%3d | TRAIN: Loss %6.3f | TEST: Loss %6.3f | TIME: %6f s'  %
                ( epoch , num_epochs , train_loss , test_loss , finish - start ) )



