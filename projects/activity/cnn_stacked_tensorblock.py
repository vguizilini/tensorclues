
import numpy as np
import tensorflow as tf
import tensorclues as tc

tf.set_random_seed( 10 )

stackLen = 3

def buildNetwork():

    rc = tc.Recipe()

    sess = tf.InteractiveSession()

    y = rc.placeholder( [ None , 9 ] )
    x = rc.placeholder( [ None , 64 * 64 * stackLen ] )

    rc.defaultVariable( stddev = 0.03 , seed = 10 , value = 0.03 )

    wgts = rc.initVariable( tc.vars.trunc_normal )
    bias = rc.initVariable( tc.vars.constant     )

    pool = tc.init( tc.extras.pool2d , ksize = 2 , strides = 2 )
    drop , relu , soft = tc.extras.dropout , tc.activs.relu , tc.activs.softmax

    rc.defaultLayer( wgts = wgts , bias = bias , post = [ relu , pool ] )

    conv1 = rc.layerConv2D( out_channels =  20 , in_sides = 64 )
    conv2 = rc.layerConv2D( out_channels =  40 )
    conv3 = rc.layerConv2D( out_channels =  70 )
    conv4 = rc.layerConv2D( out_channels = 100 )
    conv5 = rc.layerConv2D( out_channels = 130 )

    rc.defaultLayer( post = [ relu , drop ] )

    fully1 = rc.layerFully( out_channels = 1024 )
    fully2 = rc.layerFully( out_channels =  512 )

    rc.defaultLayer( post = [ soft ] )

    output = rc.layerFully( out_channels = y , name = 'Output' )

    return sess , x , y , output['x'] , fully1['dropout'] , fully2['dropout']

def evaluate(sess, accuracy, confusion, x, y_, keep_prob1, keep_prob2,
             test_data, test_labels, test_sizes, logPath):
    accuracy_list = list()
    confusion_matrix = np.zeros((9,9))
    # for each video part
    for videoId in range(test_data.shape[0]):
        video_data = np.zeros((int(test_sizes[videoId]), stackLen, 64*64))
        video_labels = np.zeros((int(test_sizes[videoId]), 9))

        # for each frame of the video
        for frameId in range(int(test_sizes[videoId]) - stackLen):
            if np.sum(test_labels[videoId, frameId]) == 0:
                continue
            stack_labels = list()
            # groups stackLen frames
            for j in range(stackLen):
                video_data[frameId,j] = test_data[videoId, frameId+j]
                stack_labels += [np.argmax(test_labels[videoId, frameId+j])]
            # label referred to the last frame of the stack
            label = stack_labels[stackLen-1]
            video_labels[frameId, label] = 1
        video_data = np.reshape(video_data, (int(test_sizes[videoId]), stackLen*64*64))
        # runs networkon test batch to obtain its accuracy and confusion matrix
        [acc_batch, cm_batch] = sess.run([accuracy, confusion], feed_dict={
                x: video_data, y_: video_labels,
                keep_prob1: 1.0, keep_prob2: 1.0})
        # concatenates accuracy and sums confusion matrix
        accuracy_list += [acc_batch]
        confusion_matrix += cm_batch
    # saves test accuracy to file
    with open(logPath, 'a') as logFile:
        logFile.write(str(np.mean(accuracy_list)) + '\n')
    # presents result
    print("test accuracy %g"%(np.mean(accuracy_list)))
    print('confusion matrix:\n', confusion_matrix)


if __name__ == '__main__':

    train_data   = np.load( 'data/train_data_clean.npy'   )
    train_labels = np.load( 'data/train_labels_clean.npy' )
    train_sizes  = np.load( 'data/train_sizes_clean.npy'  )
    test_data    = np.load( 'data/test_data_clean.npy'    )
    test_labels  = np.load( 'data/test_labels_clean.npy'  )
    test_sizes   = np.load( 'data/test_sizes_clean.npy'   )

    logPath = 'results/log_vitor_stacked.txt'

    sess , x , y_ , y_conv , keep_prob1 , keep_prob2 = buildNetwork()

    cross_entropy = -tf.reduce_sum( y_ * tf.log( tf.clip_by_value( y_conv , 1e-10 , 1.0 ) ) )
    train_step = tf.train.AdamOptimizer( 1e-5 ).minimize( cross_entropy )

    correct_prediction = tf.equal( tf.argmax( y_conv , 1 ) , tf.argmax( y_ , 1 ) )
    accuracy = tf.reduce_mean( tf.cast( correct_prediction , tf.float32 ) )
    confusion = tf.confusion_matrix( labels      = tf.argmax( y_     , 1 ) ,
                                     predictions = tf.argmax( y_conv , 1 ) ,
                                     num_classes = 9 )

    sess.run( tf.global_variables_initializer() )

    train_batch_size = 32
    for epoch in range(12501):

        np.random.seed( epoch )

        batch_data = np.zeros((train_batch_size, stackLen, 64*64))
        batch_labels = np.zeros((train_batch_size, 9))
        # selects video parts randomly to compose a batch for training
        videosId = np.random.choice(train_data.shape[0], train_batch_size)
        # for each video part selected
        for i in range(train_batch_size):
            # Select a frame of the video
            frameId = np.random.randint(0,train_sizes[videosId[i]]-stackLen)
            # The label must be a valid activity
            while np.sum(train_labels[frameId+stackLen-1]) == 0.0:
                frameId = np.random.randint(0,train_sizes[videosId[i]]-stackLen)
            stack_labels = list()
            # groups stackLen frames
            for j in range(stackLen):
                batch_data[i,j] = train_data[videosId[i], frameId+j]
                stack_labels += [np.argmax(train_labels[videosId[i], frameId+j])]
            # label referred to the last frame of the stack
            label = stack_labels[stackLen-1]
            batch_labels[i, label] = 1
        # prepare batch for training
        batch_data = np.reshape(batch_data, (train_batch_size, stackLen*64*64))
        # train the selected batch
        [_, cross_entropy_py] = sess.run([train_step, cross_entropy],
                                         feed_dict={x: batch_data, y_: batch_labels,
                                                    keep_prob1: 0.5, keep_prob2: 0.5})
        if epoch%100 == 0:
            train_accuracy = accuracy.eval(feed_dict={x:batch_data,
                                                      y_:batch_labels,
                                                      keep_prob1: 0.5, keep_prob2: 0.5})
            print('step %d, training accuracy %g, cross entropy %g'%(epoch,
                                                                     train_accuracy,
                                                                     cross_entropy_py))
        if epoch%500 == 0:
            evaluate(sess, accuracy, confusion, x, y_, keep_prob1, keep_prob2,
                         test_data, test_labels, test_sizes, logPath)
