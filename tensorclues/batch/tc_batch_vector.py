
import numpy as np

##### VECTOR
class vector:

    ### INITIALIZE
    def __init__( self , data , label = None ):

        self.data , self.label = data , label
        self.idx = np.arange( 0 , data.shape[0] )
        self.cnt = -1

    ### SHUFFLE
    def shuffle( self ):
        np.random.shuffle( self.idx )
        self.reset()

    ### RESET
    def reset( self ):
        self.cnt = -1

    ### GET NUM BATCHES
    def num_batches( self , size_batch ):
        self.size_batch = size_batch
        num_batches = int( self.idx.shape[0] / size_batch )
        if self.idx.shape[0] % size_batch > 0: num_batches += 1
        return num_batches

    ### GET BATCH
    def get( self , batch ):

        st , fn = batch * self.size_batch , ( batch + 1 ) * self.size_batch
        if fn > self.idx.shape[0]: fn = self.idx.shape[0]
        idx = self.idx[st:fn]

        if self.label is None: return self.data[idx]
        else: return self.data[idx] , self.label[idx]

    ### GET NEXT
    def next( self ):
        self.cnt += 1
        return self.get( self.cnt )




#### GET BATCH DATA (Numpy)
#def get_batch_npy( data , b , i ):

#    l = len( data )

#    st = ( i + 0 ) * b % l
#    fn = ( i + 1 ) * b % l

#    if st > fn :
#        if fn == 0: return data [ st : l ]
#        else: return np.vstack( ( data[ st : l ] , data[ 0 : fn ] ) )
#    else:
#        return data[ st : fn ]

#### GET BATCH DATA (List)
#def get_batch_lst( data , b , i ):

#    l = len( data )

#    st = ( i + 0 ) * b % l
#    fn = ( i + 1 ) * b % l

#    if st > fn :
#        if fn == 0: return data [ st : l ]
#        else: return data[ st : l ] + data[ 0 : fn ]
#    else:
#        return data[ st : fn ]

#### GET BATCH_DATA
#def get_batch( data , b , i ):

#    if isinstance( data , list ):
#        return get_batch_lst( data , b , i )

#    if isinstance( data , np.ndarray ):
#        return get_batch_npy( data , b , i )

#    print( 'DATA TYPE NOT SUPPORTED' )
#    return None
