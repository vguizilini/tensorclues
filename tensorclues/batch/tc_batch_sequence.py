
import numpy as np
import tensorclues as tc

##### SEQUENCE PREDICT FREE
class seqpred_fixed:

    ### INITIALIZE
    def __init__( self , data , seqlen , tsteps , tpreds ):

        self.data , self.seqlen , self.tsteps , self.tpreds = data , seqlen , tsteps , tpreds
        self.idx = tc.prep.idx_seqpred_fixed( data , seqlen , tsteps , tpreds )

    ### SHUFFLE
    def shuffle( self ):
        np.random.shuffle( self.idx )

    ### GET NUM BATCHES
    def size( self , size_batch ):
        self.size_batch = size_batch
        return int( self.idx.shape[0] / size_batch ) + 1

    ### GET BATCH
    def get( self , batch ):

        st , fn = batch * self.size_batch , ( batch + 1 ) * self.size_batch
        if fn > self.idx.shape[0]: fn = self.idx.shape[0] - 1

        return tc.prep.dataidx_seqpred_fixed( \
            self.data , self.seqlen , self.tsteps , self.tpreds , self.idx , st , fn )

