
import tensorflow as tf
import tensorclues as tc

### RECURRENT
def recur( rc , input , num_cells , transpose ,
           in_dropout , out_dropout ,
           seqlen , out_channels , **args ):

    x = tc.extract( input )
    if transpose: x = tf.transpose( x , [ 0 , 2 , 1 ] )

    cells = []
    for _ in range( num_cells ):
        cells.append( tf.nn.rnn_cell.BasicLSTMCell(
            out_channels , forget_bias = 1.0 , state_is_tuple = True ) )
    cell = tf.nn.rnn_cell.MultiRNNCell( cells , state_is_tuple = True )

    if in_dropout:
        in_drop = tf.placeholder( tf.float32 )
        cell = tf.nn.rnn_cell.DropoutWrapper( cell , input_keep_prob = in_drop )
    if out_dropout:
        out_drop = tf.placeholder( tf.float32 )
        cell = tf.nn.rnn_cell.DropoutWrapper( cell , output_keep_prob = out_drop )

    outputs , states = tf.nn.dynamic_rnn( cell , x , dtype = tf.float32 ,
                                          sequence_length = seqlen )

    if seqlen is None:

        shape = tc.shape( outputs )
        trans = list( range( len( shape ) ) )
        trans[0] , trans[1] = trans[1] , trans[0]

        x = tf.transpose( outputs , trans )[-1]

    else:

        batch_shape , batch_size = tc_shape( x ) , tf.shape( outputs )[0]
        index = tf.range( 0 , batch_size ) * batch_shape[1] + ( seqlen - 1 )

        x = tf.gather( tf.reshape( outputs , [ -1 , out_channels] ) , index )

    layer = { 'x' : x , 'output' : outputs , 'states' : states }
    if in_dropout: layer['in_dropout'] = in_drop
    if out_dropout: layer['out_dropout'] = out_drop

    return layer

