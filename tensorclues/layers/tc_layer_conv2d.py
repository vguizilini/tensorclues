
import tensorflow as tf
import tensorclues as tc

### CONVOLUTION 2D
def conv2d( rc , input , wgts , bias , ksize , strides ,
                 in_sides , in_channels , out_sides , out_channels , **args ):

    x = tc.extract( input )
    x , in_sides , in_channels = tc.aux.fold2D( x , in_sides , in_channels )

    W = rc.new_variable( wgts , [ ksize , ksize , in_channels , out_channels ] )
    b = rc.new_variable( bias , [ out_channels ] )

    strides = [ 1 , strides , strides , 1 ]

    x = tf.nn.conv2d( x , W , strides = strides , padding = 'SAME' )
    x = tc.addBias( x , b )

    return { 'x' : x , 'wgts' : W , 'bias' : b }


