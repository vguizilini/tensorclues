
import tensorflow as tf
import tensorclues as tc

### DECONVOLUTION 2D
def deconv2d( rc , input , wgts , bias , ksize , strides ,
                   in_sides , in_channels , out_sides , out_channels , **args ):

    x = tc.extract( input )
    x , in_sides , in_channels = tc.aux.fold2D( x , in_sides , in_channels )

    W = rc.new_variable( wgts , [ ksize , ksize , out_channels , in_channels ] )
    b = rc.new_variable( bias , [ out_channels ] )

    if out_sides is None:
        out_sides = [ int( tc.shape( x )[1] * strides ) ,
                      int( tc.shape( x )[2] * strides ) ]

#    output_shape = [ 64 , out_sides[0] , out_sides[1] , out_channels ]
    output_shape = [ tf.shape( x )[0] , out_sides[0] ,
                                        out_sides[1] , out_channels ]

    strides = [ 1 , strides , strides , 1 ]

    x = tf.nn.conv2d_transpose( x , W , strides = strides , output_shape = output_shape )
    x = tf.reshape( x , output_shape )
    x = tc.addBias( x , b )

    return { 'x' : x , 'wgts' : W , 'bias' : b }


