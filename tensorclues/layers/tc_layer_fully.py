
import tensorflow as tf
import tensorclues as tc

### FULLY CONNECTED
def fully( rc , input , wgts , bias ,
           out_channels , **args ):

    x = tc.flatten( tc.extract( input ) )

    W = rc.new_variable( wgts , [ tc.shape( x )[1] , out_channels ] )
    b = rc.new_variable( bias , [ out_channels ] )

    x = tf.matmul( x , W )
    x = tc.addBias( x , b )

    return { 'x' : x , 'wgts' : W , 'bias' : b }

