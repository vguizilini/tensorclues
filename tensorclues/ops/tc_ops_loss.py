
import numpy as np
import tensorflow as tf
import tensorclues as tc

### MEAN SQUARED ERROR
def mean_squared_error( input1 , input2 ):
    x1 , x2 = tc.extract( input1 ) , tc.extract( input2 )
    return tf.losses.mean_squared_error( x1 , x2 )
