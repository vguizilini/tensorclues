
import tensorflow as tf

### Adam Optimizer
def adam( learning_rate , **args ):
    return tf.train.AdamOptimizer( learning_rate )

### Gradient Descent Optimizer
def grad_descent( learning_rate , **args ):
    return tf.train.GradientDescentOptimizer( learning_rate )
