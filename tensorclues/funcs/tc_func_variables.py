
import tensorflow as tf
import tensorclues as tc

### BASE VARIABLE
def var_base( init , **args ):
    if args['name'] is None:
        return tf.Variable( initial_value = init ,
                name = args['name'] , trainable = args['trainable'] )
    else:
        return tf.get_variable( initializer = init ,
                name = args['name'] , trainable = args['trainable'] )

### PLACEHOLDER
def placeholder( shape , name , dtype , **args ):
    return tf.placeholder( shape = shape , dtype = dtype )

### NUMPY
def numpy( shape , dtype , **args ):
    init = tf.constant( shape , dtype = dtype )
    return var_base( init , **args )

### TRUNCATED NORMAL
def trunc_normal( shape , dtype ,
                  mean , stddev , seed , **args ):
    init = tf.truncated_normal( shape = shape , dtype = dtype ,
            mean = mean , stddev = stddev , seed = seed )
    return var_base( init , **args )

### XAVIER
def xavier( shape , dtype ,
            seed , **args ):
    init = tf.random_normal( shape = shape , dtype = dtype ,
            stddev = 1.0 / tf.sqrt( shape[0] / 2.0 ) , seed = seed )
    return var_base( init , **args )

### CONSTANT
def constant( shape , dtype ,
              value , **args ):
    init = tf.constant( shape = shape , dtype = dtype ,
            value = value )
    return var_base( init , **args )
