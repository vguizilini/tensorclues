
import tensorflow as tf
import tensorclues as tc

### MEAN SOFT CROSS LOGITS
def mean_soft_cross( logits , labels , **args ):

    return tf.reduce_mean( tf.nn.softmax_cross_entropy_with_logits(
                logits = tc.extract( logits ) , labels = tc.extract( labels ) ) )

### MEAN SOFT CROSS LOGITS
def mean_sigm_cross( logits , labels , **args ):

    return tf.reduce_mean( tf.nn.sigmoid_cross_entropy_with_logits(
                logits = tc.extract( logits ) , labels = tc.extract( labels ) ) )

### MEAN SQUARED ERROR
def mean_squared_error( input1 , input2 , **args ):
    return tf.losses.mean_squared_error( 
		tc.extract( input1 ) , tc.extract( input2 ) )

### Mean Equal Argmax
def perc_correct( input1 , input2 , **args ):

    correct = tf.equal( tf.argmax( tc.extract( input1 ) , 1 ) ,
                        tf.argmax( tc.extract( input2 ) , 1 ) )

    return tf.reduce_mean( tf.cast( correct , tf.float32 ) )
