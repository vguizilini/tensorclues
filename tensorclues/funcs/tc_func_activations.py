
import tensorflow as tf
import tensorclues as tc

### RECTIFIED LINEAR UNIT
def relu( input , **args ):

    x = tf.nn.relu( tc.extract( input ) )
    return tc.merge( input , { 'x' : x } )

### LEAKY RECTIFIED LINEAR UNIT
def lrelu( input , leak , **args ):

    x = tc.extract( input )
    x = tf.maximum( x , leak * x )
    return tc.merge( input , { 'x' : x } )

### SOFTMAX
def softmax( input , **args ):

    x = tf.nn.softmax( tc.extract( input ) )
    return tc.merge( input , { 'x' : x } )

### SIGMOID
def sigmoid( input , **args ):

    x = tf.nn.sigmoid( tc.extract( input ) )
    return tc.merge( input , { 'x' : x } )
