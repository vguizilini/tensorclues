
import numpy as np

### GET DATA SEQLEN
def get_data_seqlen( data ):

    data_seqlen = []

    max_seqlen = 0
    for i in range( len( data ) ):

        seqlen = len( data[i] )
        data_seqlen.append( seqlen )

        if seqlen > max_seqlen:
            max_seqlen = seqlen

    return data_seqlen

### PAD DATA
def pad_data( data , max_seqlen = None ):

    data_seqlen = get_data_seqlen( data )
    if max_seqlen is None: max_seqlen = max( data_seqlen )

    max_featlen = len( data[0][0] )
    pad = [ 0.0 for _ in range( max_featlen ) ]

    for i in range( len( data ) ):
        data[i] += [ pad for _ in range( max_seqlen - data_seqlen[i] ) ]

    return data_seqlen

### PAD PAIR
def pad_pair( data , labels , max_seqlen = None ):

    data_seqlen = pad_data( data , max_seqlen )
    pad_data( labels , max_seqlen )
    return data_seqlen

### PAD DATA (Return as Numpy)
def pad_data_npy( data , max_seqlen = None ):

    data_seqlen = pad_data( data , max_seqlen )
    return np.array( data ) , np.array( data_seqlen )

### PAD PAIR (Return as Numpy)
def pad_pair_npy( data , labels , max_seqlen = None ):

    data_seqlen = pad_pair( data , labels , max_seqlen )
    return np.array( data ) , np.array( labels ) , np.array( data_seqlen )
