
import pickle
import numpy as np

### SAVE LIST
def save_lst( file , lst ):
    pickle.dump( lst , open( file + '.lst' , 'wb' ) )

### SAVE NUMPY
def save_npy( file , npy ):
    np.save( file + '.npy' , npy )
