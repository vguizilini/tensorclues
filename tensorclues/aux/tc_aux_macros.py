
import numpy as np
import tensorflow as tf

### PARTIAL INITIALIZE
def init( func , *args , **keywords ):
    def newfunc( *fargs , **fkeywords ):
#        newkeywords = fkeywords.copy() ; newkeywords.update( keywords )
        newkeywords = keywords.copy() ; newkeywords.update( fkeywords )
        return func( *( args + fargs ) , **newkeywords )
    newfunc.func , newfunc.args , newfunc.keywords = func , args , keywords
    return newfunc

### EXTRACT INPUT
def extract( input ):
    return input['x'] if isinstance( input , dict ) else input

### MERGE OUTPUT
def merge( input , output ):
    if isinstance( input , dict ):
        input = input.copy()
        input.update( output )
        return input
    return output

### GET SHAPE
def shape( x ):
    if isinstance( x , tuple ): return np.shape( x )
    return x.get_shape().as_list()
def pshape( x ): print( shape( x ) )

### GET LENGTH
def length( x ):
    return len( shape( x ) )

### FLATTEN
def flatten( x ):
    if length( x ) > 2:
        x = tf.reshape( x , [ -1 , np.prod( shape( x )[1:] ) ] )
    return x

### ADD BIAS TO LAYER
def addBias( x , b ):
    return x if b is None else tf.add( x , b )

### TO LIST
def tolist( x ):
    return x if isinstance( x , list ) else [ x ]

### ADD SCOPE NAME
def addScopeName( scope , name ):
    if name is not None:
        if scope is not '': scope += '/'
        scope = scope + name
    return scope

### MERGE DICTS
def merge_dicts( dict1 , dict2 ):
    return { **dict1 , **dict2 }

### CHECKS

def isString( x ): return isinstance( x , str        )
def isList(   x ): return isinstance( x , list       )
def isDict(   x ): return isinstance( x , dict       )
def isNumpy(  x ): return isinstance( x , np.ndarray )
def isTensor( x ): return isinstance( x , tf.Tensor  ) or isinstance( x , tf.Variable )
def isObject( x ): return isinstance( x , object     ) and not isTensor( x ) and not isNumpy( x )
def isInit(   x ): return isObject( x ) and 'keywords' in x.__dict__
def isVars(   x ): return isObject( x ) and 'keywords' not in x.__dict__

### CONDITIONAL SCOPE

class empty_scope():
     def __init__( self  ): pass
     def __enter__ (self ): pass
     def __exit__(  self , type , value , traceback ): pass
def cond_scope( scope ):
    return empty_scope() if scope is None else tf.variable_scope( scope )

