
import numpy as np
import tensorflow as tf
import tensorclues as tc

### FOLD 2D
def fold2D( x , in_sides , in_channels ):

    shape = tc.shape( tc.extract( x ) )

    if len( shape ) < 4:
        if in_sides is None and in_channels is None:
            raise Exception( 'Fold2D ERROR! NO FOLDING DIMENSIONS PROVIDED' )
        if in_sides is None:
            in_sides = int( np.sqrt( shape[1] / in_channels ) )
            in_sides = 2 * [ in_sides ]
        else:
            if not tc.isList( in_sides ): in_sides = 2 * [ in_sides ]
            in_channels = int( shape[1] / np.prod( in_sides ) )
        x = tf.reshape( x , [ -1 , in_sides[0] , in_sides[1] , in_channels ] )
    else:
        in_sides , in_channels = shape[1:3] , shape[-1]

    return x , in_sides , in_channels
