
import os
import tensorflow as tf
import tensorclues as tc

class RecipeSaver:

    ### CREATE SAVER
    def saver( self , vars = None , path = None ,
                name = None , max_to_keep = 1 ):

        if path is not None: path += '/model'

        if vars is None:
            vars = self.get_global()
        elif tc.checks.isString( vars ):
            vars = self.get_global( vars )

        saver = tf.train.Saver( vars ,
                        name = name , max_to_keep = max_to_keep )

        self.checkAdd( saver , name )
        return dict( saver = saver , path = path )

    ### PERFORM SAVE
    def save( self , saver , path = None , step = None ):
        path = saver['path'] if path is None else path + '/model'
        if not os.path.exists( path ): os.makedirs( path )
        saver['saver'].save( self.sess , save_path = path , global_step = step )

    ### PERFORM RESTORE
    def restore( self , saver , path = None ):
        path = saver['path'] if path is None else path + '/model'
        if os.path.exists( path ): saver['saver'].restore( self.sess , path )


