
import tensorflow as tf
import tensorclues as tc

class RecipeVariable:

    ### ADD VARIABLE
    def variable( self , type , shape = None ,
                  in_scope = True , **args ):

        if tc.isTensor( type ): return type

        if not tc.isInit( type ): pars = { **self.dict_variable , **args }
        else: pars = { **self.dict_variable , **type.__dict__['keywords'] , **args }

        with tc.cond_scope( self.curr_scope if in_scope else None ):

            name = tc.addScopeName( tf.get_variable_scope().name , pars['name'] )
            self.reuse( len( self.get_global( name ) ) > 0 )

            if shape is not None and pars['first_none']:
                shape = list( shape ) ; shape[0] = None

            if tc.isNumpy( type ):
                var = tc.vars.numpy( type , **pars )
            else: var = type( shape , **pars )

        self.checkAdd( var , pars )
        return var

    ### NEW VARIABLE
    def new_variable( self , type , shape ):

        if tc.isTensor( type ): return type
        if tc.isNumpy(  type ): args = {}
        if tc.isVars(   type ): args = {}
        if tc.isInit(   type ): args = type.__dict__['keywords']
        return self.variable( type , shape , in_scope = False , **args )

    ### ADD PLACEHOLDER
    def placeholder( self , shape , **args ):
        self.last_layer = self.variable( tc.vars.placeholder , shape , **args )
        return self.last_layer

