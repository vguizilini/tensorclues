
import tensorclues as tc

class RecipeOperation:

    ### ADD OPERATION
    def operation( self , type , **args ):        
        return self.optimizer( type , None , **args )

    ### ADD OPTIMIZER
    def optimizer( self , type , loss = None , **args ):

        pars = { **self.dict_operation , **args }

        with tc.cond_scope( self.curr_scope ):

            optim = type( **pars )
            if loss is not None:
                optim = optim.minimize( loss )

        self.checkAdd( optim , pars )
        return optim
