
import tensorflow as tf
import tensorclues as tc

class RecipeDefault:

    ### SET INIT DEFAULTS
    def initDefault( self ):

        self.dict_variable = dict(
            name = None , first_none = False ,
            mean = 0.0 , stddev = 0.1 , value = 0.0 ,
            seed = None , trainable = True , dtype = tf.float32 ,
        )

        self.dict_layer = dict(
            name = None , input = None , post = None ,
            ksize = 5 , strides = 1 , num_cells = 1 ,
            in_sides = None , in_channels = None ,
            out_sides = None , out_channels = None ,
            seqlen = None , transpose = False ,
            in_dropout = False , out_dropout = False ,
            wgts = self.initVariable( tc.vars.trunc_normal ) ,
            bias = self.initVariable( tc.vars.trunc_normal ) ,
        )

        self.dict_operation = dict(
            name = None ,
            learning_rate = 1e-5 ,
            logits = None , labels = None ,
        )

    ### SET DEFAULTS
    def defaultVariable(  self , **pars ): self.dict_variable  = { **self.dict_variable  , **pars }
    def defaultLayer(     self , **pars ): self.dict_layer     = { **self.dict_layer     , **pars }
    def defaultOperation( self , **pars ): self.dict_operation = { **self.dict_operation , **pars }

    ### SET DEFAULTS
    def defVar(   self , **pars ): self.defaultVariable(  **pars )
    def defLayer( self , **pars ): self.defaultLayer(     **pars )
    def defOp(    self , **pars ): self.defaultOperation( **pars )
