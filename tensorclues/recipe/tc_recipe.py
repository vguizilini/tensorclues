
import tensorflow as tf
import tensorclues as tc

class Recipe( tc.recipe.Default  , tc.recipe.Initialize ,
              tc.recipe.Variable , tc.recipe.Layer , tc.recipe.Operation ,
              tc.recipe.Access   , tc.recipe.Saver ):

    ### CONSTRUCTOR
    def __init__( self ):

        self.initDefault()

        config = tf.ConfigProto( allow_soft_placement = False )
        self.sess = tf.Session( config = config )

        self.last_layer = None
        self.added , self.curr_scope = {} , ''

    ### CHECK TO ADD
    def checkAdd( self , x , pars ):
        if tc.isDict( pars ): pars = pars['name']
        if pars is not None: self.added[ pars ] = x

    ### SCOPE
    def scope( self , scope = '' , reuse = False ):
        self.curr_scope = scope
        self.reuse( reuse )

    ### REUSE
    def reuse( self , flag ):
        tf.get_variable_scope()._reuse = flag

    ### ADD SCOPE
    def add_scope( self , scope ):
        if len( self.curr_scope ) > 0:
            self.curr_scope += '/'
        self.curr_scope += scope

    ### BACK SCOPE
    def back_scope( self , n = 1 ):
        list = self.curr_scope.split( '/' )
        if len( list ) <= n:
            self.curr_scope = ''
        else:
            self.curr_scope = list[0]
            for l in list[1:-n]: self.curr_scope += '/' + l








