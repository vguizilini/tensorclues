
import tensorclues as tc

class RecipeLayer:

    ### ADD LAYER
    def layer( self , type , **args ):

        pars = { **self.dict_layer , **args }

        scope = tc.addScopeName( self.curr_scope , pars['name'] )
        with tc.cond_scope( scope ):

            pars = self.prepPars( pars )

            layer = type( self , **pars )
            layer = self.runPostOps( layer , pars['post'] )
            layer['pars'] = pars

        self.checkAdd( layer , pars )
        self.last_layer = layer
        return layer

    ### ADD LAYER SHORTCUTS
    def Fully(    self , **pars ): return self.layer( tc.layers.fully    , **pars )
    def Conv2D(   self , **pars ): return self.layer( tc.layers.conv2d   , **pars )
    def Deconv2D( self , **pars ): return self.layer( tc.layers.deconv2d , **pars )
    def Recur(    self , **pars ): return self.layer( tc.layers.recur    , **pars )

    ### RUN POST OPERATIONS
    def runPostOps( self , layer , post ):
        if post is not None:
            for op in tc.tolist( post ): layer = op( layer )
        return layer

    ### PREPARE PARAMS
    def prepPars( self , pars ):

        if pars['input'] is None: pars['input'] = self.last_layer
        pars['input'] = tc.extract( pars['input'] )

        if tc.isTensor( pars['out_channels'] ):
            pars['out_channels'] = tc.shape( pars['out_channels'] )[-1]

        return pars



