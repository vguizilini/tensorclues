
import tensorflow as tf

class RecipeAccess:

############################## ADDED

    def __getitem__( self , key ):
        return self.added[ key ]

############################## RUN SESSION

    ### RUN IN SESSION
    def run( self , ops , dict = None ):
        return self.sess.run( ops , dict )

    ### RUN IN SESSION AND PRINT
    def prun( self , ops , dict = None ):
        print( self.run( ops , dict ) )

############################## VARIABLES

    ### GLOBAL
    def get_global( self , scope = None ):
        return tf.get_collection( tf.GraphKeys.GLOBAL_VARIABLES , scope = scope )

    ### TRAINABLE
    def get_trainable( self , scope = None ):
        return tf.get_collection( tf.GraphKeys.TRAINABLE_VARIABLES , scope = scope )

    ### GLOBAL NAMES
    def get_global_names( self , scope = None ):
        vars = self.get_global( scope )
        return [ var.name for var in vars ]

    ### TRAINABLE NAMES
    def get_trainable_names( self , scope = None ):
        vars = self.get_trainable( scope )
        return [ var.name for var in vars ]

    ### PRINT GLOBAL
    def print_global( self , scope = None ):
        vars = self.get_global( scope )
        print( '##### GLOBAL VARIABLES #####' )
        for var in vars: print( var.name , '-' , var.shape )
        print( '############################' )

    ### PRINT TRAINABLE
    def print_trainable( self , scope = None ):
        vars = self.get_trainable( scope )
        print( '##### TRAINABLE VARIABLES #####' )
        for var in vars: print( var.name , '-' , var.shape )
        print( '###############################' )
