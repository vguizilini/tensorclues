
import tensorflow as tf
import tensorclues as tc

class RecipeInitialize:

############################## INDIVIDUAL INITIALIZERS

    ### VARIABLE INITIALIZE
    def initVariable( self , fn , **args ):
        pars = { **self.dict_variable , **args }
        return tc.init( fn , **pars )
    def initVar( self , fn , **args ):
        return self.initVariable( fn , **args )

    ### LAYER INITIALIZE
    def initLayer( self , fn , **args ):
        pars = { **self.dict_layer , **args }
        return tc.init( fn , **pars )

    ### OPERATION INITIALIZE
    def initOperation( self , fn , **args ):
        pars = { **self.dict_operation , **args }
        return tc.init( fn , **pars )
    def initOp( self , fn , **args ):
        return self.initOperation( fn , **args )

############################## GRAPH INITIALIZERS

    ### INITIALIZE VARIABLES
    def initializer( self , vars = None ):
        if vars is None: return tf.global_variables_initializer()
        if tc.isString( vars ): vars = self.global_variables( vars )
        return tf.variables_initializer( tc.tolist( vars ) )

    ### INITIALIZE VARIABLES
    def initialize( self , vars = None ):
        self.sess.run( self.initializer( vars ) )



