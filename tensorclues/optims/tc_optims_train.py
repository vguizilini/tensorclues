
import numpy as np
import tensorflow as tf
import tensorclues as tc

### ADAM OPTIMIZER
def adam( learning_rate , **args ):
    return tf.train.AdamOptimizer( learning_rate )

### GRADIENT DESCENT OPTIMIZER
def gradient_descent( learning_rate , **args ):
    return tf.train.GradientDescentOptimizer( learning_rate )
