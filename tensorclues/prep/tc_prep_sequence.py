
import numpy as np

### IDX SEQUENCE PREDICT FIXED
def idx_seqpred_fixed( data , seqlen , tsteps , tpreds , st = None , fn = None ):

    if st is None: st = 0
    if fn is None: fn = data.shape[0]

    idx = []
    for i in range( st , fn ):
        for j in range( tsteps + 1 , seqlen[i] - tpreds[1] + 1 ):
            idx.append( [ i , j ] )
    return np.array( idx )

### DATA SEQUENCE PREDICT FIXED
def data_seqpred_fixed( data , seqlen , tsteps , tpreds , st = None , fn = None ):

    if st is None: st = 0
    if fn is None: fn = data.shape[0]

    data_vector , label_vector = [] , []
    for i in range( st , fn ):
        for j in range( tsteps + 1 , seqlen[i] - tpreds[1] + 1 ):
            data_vector.append(  data[i][ j - tsteps    - 1 : j             ] )
            label_vector.append( data[i][ j + tpreds[0] - 1 : j + tpreds[1] ] )
    return np.array( data_vector ) , np.array( label_vector )

### DATAIDX SEQUENCE PREDICT FIXED
def dataidx_seqpred_fixed( data , seqlen , tsteps , tpreds , idx , st , fn ):

    data_vector , label_vector = [] , []
    for k in range( st , fn ):
        i , j = idx[ k ]
        data_vector.append(  data[i][ j - tsteps    - 1 : j             ] )
        label_vector.append( data[i][ j + tpreds[0] - 1 : j + tpreds[1] ] )
    return np.array( data_vector ) , np.array( label_vector )


