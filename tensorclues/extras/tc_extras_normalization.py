
import tensorflow as tf
import tensorclues as tc

### BATCH_NORMALIZATION
def batch_normalization( input , **args ):

    out_channels = input.get_shape()[-1]

    beta  = tf.get_variable( 'BN_Beta' , [ out_channels ] , initializer = tf.zeros_initializer() )
    gamma = tf.get_variable( 'BN_Gamma', [ out_channels ] , initializer =  tf.ones_initializer() )

    if len( input.get_shape() ) == 2:
        mean , var = tf.nn.moments( input , [ 0 ] )
    else:
        mean , var = tf.nn.moments( input , [ 0 , 1 , 2 ] )

    return tf.nn.batch_normalization( input , mean , var , beta , gamma , 1e-5 )

