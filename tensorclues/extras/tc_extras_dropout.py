
import tensorflow as tf
import tensorclues as tc

### DROPOUT
def dropout( input , **args ):

    drop = tf.placeholder( tf.float32 )
    x = tf.nn.dropout( tc.extract( input ) , drop )
    return tc.merge( input , { 'x' : x , 'dropout' : drop } )
