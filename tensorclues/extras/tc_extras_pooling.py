
import tensorflow as tf
import tensorclues as tc

### POOLING 2D
def pool2D( input , ksize , strides , **args ):

    x = tc.extract( input )

    ksize = [ 1 , ksize , ksize , 1 ]
    strides = [ 1 , strides , strides , 1 ]

    x = tf.nn.max_pool( x , ksize = ksize ,
                            strides = strides ,
                            padding = 'SAME' )

    return tc.merge( input , { 'x' : x } )
